BeatBox
=======

Fully asynchrone modular application framework for Node.js.

Fork of https://github.com/coolony/metronome.


Documentation
-------------

See the `test` directory.


Testing
-------

To run the tests:

    npm install
    npm test

The `test` directory contains a fully-configured application with a `mocha`
module. It serves both as a sample module and as a test framework (it is the
recommended way to run tests for applications built with BeatBox).
