/**
 * Module dependencies.
 */

var path = require('path');
var fs = require('fs');
var EventEmitter = require('events').EventEmitter;
var util = require('util');
var _ = require('underscore');
var async = require('async');


/**
 * Module constants.
 */

var packagePathCache = {};


/**
 * `BeatBox` constructor.
 */

function BeatBox(settings) {
  EventEmitter.call(this);
  this.settings = settings || {};
  this._packages = {};
  this._queue = [];
  this._busy = false;
  this._services = {};
}

util.inherits(BeatBox, EventEmitter);


/**
 * `BeatBox` prototype.
 */

_.extend(BeatBox.prototype, {

  /**
   * Register module with static config.
   */

  register: function register(config, base) {
    this._enqueue(function (callback) {
      this._register(config, base, function (err) {
        if (err) return _this.emit('err', err);
        return callback();
      });
    });
    return this;
  },


  /**
   * Register modules from config file.
   */

  config: function config(configFile) {
    var _this = this;
    _this._enqueue(function (callback) {
      require(configFile).forEach(function (config) {
        _this._register(config, path.dirname(configFile), function (err) {
          if (err) return _this.emit('err', err);
          return callback();
        });
      });
    });
    return this;
  },


  /**
   * Register modules by scanning a directory containing config files.
   */

  scan: function scan(directory) {
    var _this = this;
    _this._enqueue(function (callback) {

      // Make directory absolute.
      if (directory[0] === '.') {
        directory = path.join(_this.get('root directory') || process.cwd(),
                             directory);
      }

      // List files.
      fs.readdir(directory, function (err, names) {
        if (err) return _this.emit('error', err);

        // Load each in order.
        function iterator(name, next) {
          if (name[0] === '.' && _this.get('ignore hidden files') !== false) {
            return next();
          }
          var absolutePath = path.join(directory, name);
          try {
            var required = require(absolutePath);
          }
          catch (err) {
            return _this.emit('error', err);
          }

          // Register it.
          return _this._register(required, directory, function (err) {
            if (err) return _this.emit('error', err);
            return next();
          });
        }

        function done(err) {
          if (err) return _this.emit('error', err);
          return callback();
        }

        return async.each(names, iterator, done);
      });
    });
    return _this;
  },


  /**
   * Start app.
   */

  bootstrap: function start() {
    var _this = this;
    _this._enqueue(function (callback) {

      // Prepare process.
      var remaining = [];
      var done = [];
      var reverse = {};
      _.each(_this._packages, function (data, name) {
        remaining.push(name);
        _.each(data.provides, function (service) {
          reverse[service] = name;
        });
      });

      // Check for missing dependencies, and check whether we can satisfy
      // weak dependencies.
      var consumes = {};
      try {
        _.each(_this._packages, function (data, name) {
          var missingDependencies = [];
          var packageConsumes = [];
          _.each(data.consumes, function (service) {
            if (!reverse[service]) return missingDependencies.push(service);
            return packageConsumes.push(service);
          });
          if (missingDependencies.length) {
            throw Error([
              'Can\'t bootstrap package "', name,
              '" because of missing dependencies: "',
              missingDependencies.join('", "'), '".'
            ].join(''));
          }
          _.each(data.optionallyConsumes, function (service) {
            if (reverse[service]) packageConsumes.push(service);
          });
          consumes[name] = packageConsumes;
        });
      }
      catch (err) {
        return _this.emit('error', err);
      }

      // Helper checking whether we may process loading some package now.
      var canLoad = function canLoad(name) {
        var packageConsumes = consumes[name];
        var missing = _.filter(packageConsumes, function (service) {
          if (!_this._services[service]) return true;
        });
        return !missing.length;
      };

      // Helper bootstraping a specific package.
      var setup = function setup(name, done) {
        _this.emit('setup', name);
        var packageData = _this._packages[name];
        var config = packageData.config;

        var _setup = null;
        if (packageData.setup.length === 3) {
          _setup = packageData.setup.bind(null, config, _this._services);
        }
        else {
          _setup = packageData.setup.bind(null, config, _this._services, _this);
        }

        _setup(function (err, services) {
          if (err) return done(err);
          services = services || {};
          _.each(packageData.provides, function (service) {
            _this._services[service] = services[service] || {};
          });
          return done();
        });
      };

      // Start a big dirty infinite loop to start with the best order.
      var iterate = function iterate() {
        var newRemaining = [];
        async.each(remaining, function (remainingItem, next) {
          if (!canLoad(remainingItem)) {
            newRemaining.push(remainingItem);
            next();
          }
          else {
            setup(remainingItem, function (err) {
              next(err);
            });
          }
        }, function (err) {
          if (err) return _this.emit('error', err);

          // Maybe we're done
          if (!newRemaining.length) {
            _this.emit('bootstraped');
            return callback();
          }

          // Dit we reach a deadlock?
          if (newRemaining.length === remaining.length) {
            return _this.emit('error', Error([
              'Can\'t bootstrap because of circular dependency in packages: "',
              remaining.join('", "'), '".'
            ].join('')));
          }

          // Let's go for another round!
          remaining = newRemaining;
          return process.nextTick(iterate);
        });
      };

      // Start the thing.
      return iterate();

    });
    return _this;
  },


  /**
   * Get settings.
   */

  get: function get(key) {
    return this.settings[key];
  },


  /**
   * Set settings.
   */

  set: function set(key, value) {
    this.settings[key] = value;
    return this;
  },


  /**
   * Get service.
   */

  service: function service(name) {
    return this._services[name];
  },


  /**
   * Get module.
   */

  module: function (name) {
    return this._packages[name];
  },


  /**
   * Get modules.
   */

  modules: function () {
    return this._packages;
  },


  /**
   * Enqueue arbitraity function.
   */

  then: function then(fn) {
    var _this = this;
    _this._enqueue(function (callback) {
      if (fn.length) {
        return fn(function (err) {
          if (err) return _this.emit('error', err);
          return callback();
        });
      }
      else {
        try {
          fn();
        }
        catch (err) {
          return _this.emit('error', err);
        }
        return callback();
      }
    });
    return _this;
  },


  /**
   * Quit method.
   */
  quit: function (status) {
    // TODO Give a chance to modules to give up.
    process.exit(status);
  },

  /**
   * Register method.
   */

  _register: function _register(config, root, callback) {
    var _this = this;
    config = typeof config === 'string' ? { packagePath: config } : config;
    if (config.enabled === false) return callback();
    return resolvePackage(config.packagePath, root, function (packagePath) {
      if (!packagePath) {
        return callback(Error([
          'Can\'t locate package "', config.packagePath, '".'
        ].join('')));
      }
      var manifest = require(path.join(packagePath, 'package.json'));
      var name = manifest.name || path.basename(packagePath);
      var indexFile = manifest.main || 'index.js';
      var setup = require(path.join(packagePath, indexFile));
      _this._packages[name] = {
        path: packagePath,
        setup: setup,
        provides: manifest.provides || [],
        consumes: manifest.consumes || [],
        optionallyConsumes: manifest.optionallyConsumes || [],
        config: config
      };
      return callback();
    });
  },


  /**
   * Processing queue.
   */

  _enqueue: function _enqueue(fn) {
    var _this = this;

    var callNext = function (err) {
      if (!_this._queue.length) {
        _this._busy = false;
      }
      else {
        process.nextTick(function () {
          (_this._queue.shift())(callNext);
        });
      }
    };

    if (!_this._busy) {
      _this._busy = true;
      fn(callNext);
    }
    else {
      _this._queue.push(fn);
    }

    return _this;
  }

});


/**
 * Factory.
 */

module.exports = function factory(options) {
  return new BeatBox(options);
};


/**
 * Expose `BeatBox`.
 */

module.exports.BeatBox = BeatBox;


/**
 * Expose version.
 */

module.exports.__defineGetter__('version', function () {
  return require(path.join(__dirname, 'package.json')).version;
});


/**
 * Node style, resolve package.
 */

function resolvePackage(packagePath, base, callback) {
  var tryPaths = [];
  if (packagePath[0] === '/') {
    tryPaths.push(packagePath);
  } else if (packagePath[0] === '.') {
    tryPaths.push(path.join(base, packagePath));
  } else {
    var tmpBase = base;
    while (true) {
      tryPaths.push(path.join(tmpBase, 'node_modules', packagePath));
      if (tmpBase === (tmpBase = path.dirname(tmpBase))) break;
    }
  }

  async.detect(tryPaths, isPackagePath, callback);
}

function isPackagePath(packagePath, callback) {
  fs.exists(path.join(packagePath, 'package.json'), callback);
}
