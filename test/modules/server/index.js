var http = require('http');

module.exports = function (settings, imports, register) {

  var server = http.createServer(function (req, res) {
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });

    res.end('{"message": "ok"}');
  });

  var api = {

    port: null,
    host: null,

    server: server,

    start: function (port, host) {
      this.port = port || process.env.PORT || 9999;
      this.host = host || process.env.HOST || '127.0.0.1';

      this.port = parseInt(this.port);

      this.server.listen(this.port, this.host);
    }

  };

  register(null, {
    server: api
  });

};
