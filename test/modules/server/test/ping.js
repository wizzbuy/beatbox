var should = require('should');
var superagent = require('superagent');

describe('Module `server`', function () {

  var imports = this.ctx.imports;
  var server = imports.server;

  var url = 'http://' + server.host + ':' + server.port;

  it('should be running.', function (done) {

    var agent = superagent.agent();

    agent
      .get(url)
      .end(function (err, res) {
        if (err) return done(err);

        res.should.have.status(200);
        res.should.be.json;

        done();
      });
  });

});
