var should = require('should');

describe('Module `server`', function () {

  var imports = this.ctx.imports;

  it('should provide `server`.', function (done) {
    imports.should.have.property('server');

    done();
  });

});
