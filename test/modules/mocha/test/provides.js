var should = require('should');

describe('Module `mocha`', function () {

  var imports = this.ctx.imports;

  it('should provide `mocha`.', function (done) {
    imports.should.have.property('mocha');

    done();
  });

});
