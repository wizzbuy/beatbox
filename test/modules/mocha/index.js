var fs = require('fs');
var _ = require('underscore');
var Mocha = require('mocha');

var defaults = {
  ui: 'exports',
  reporter: 'spec',
  bail: true,
  timeout: 2000,
  ignoreLeaks: false,
  globals: [],
  slow: 75
};


module.exports = function (settings, imports, app, register) {

  var api = {
    discover: function (callback) {
      var mocha = new Mocha();

      // Build options dynamically, override defaults as needed.
      var options = _.extend({}, defaults, settings.mocha);

      // Set options on the mocha instance.
      if (options.ui)           mocha.ui(options.ui);
      if (options.reporter)     mocha.reporter(options.reporter);
      if (options.bail)         mocha.suite.bail(options.bail);
      if (options.timeout)      mocha.suite.timeout(options.timeout);
      if (options.ignoreLeaks)  mocha.ignoreLeaks();
      if (options.globals)      mocha.globals(options.globals);
      if (options.slow)         mocha.slow();

      // Expose application attributes so test cases feel like BeatBox modules.
      mocha.suite.ctx.settings = settings;
      mocha.suite.ctx.imports = imports;
      mocha.suite.ctx.app = app;

      // Register test files for loaded modules.
      _.each(app.modules(), function (module) {
        var path = module.path + '/' + settings.directory;

        if (!fs.existsSync(path))
          return;

        _.each(fs.readdirSync(path), function (item) {
          if (!/\.js$/.test(item))
            return;

          mocha.addFile(path + '/' + item);
        });
      });

      // Run the test suite.
      mocha.run(function (failures) {
        // Pass test details over to the callback if available.
        if (_.isFunction(callback)) callback(failures);
      });
    }
  };

  // Expose the tests API.
  register(null, {
    mocha: api
  });

};
