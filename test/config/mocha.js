module.exports = {
  packagePath: '../modules/mocha',

  directory: 'test',

  mocha: {
    ui: 'exports',
    reporter: 'spec',
    bail: true
  }
};
