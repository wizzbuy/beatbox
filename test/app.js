var beatbox = require('../beatbox');


var app = beatbox()
  .scan([__dirname, 'config'].join('/'))
  .bootstrap()
  .then(function () {
    app.service('server').start();
  })
  .then(function () {
    app.service('mocha').discover(function (failures) {
      app.quit(failures && 1 || 0);
    });
  });
